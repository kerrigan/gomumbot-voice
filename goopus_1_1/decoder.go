package goopus_1_1

//#include <opus/opus.h>
//#cgo LDFLAGS: -lopus -Bsymbolic
import "C"

import (
	"fmt"
)

type Opus11Decoder struct {
	state *C.OpusDecoder
}

func NewOpus11Decoder() Opus11Decoder {
	decoder := Opus11Decoder{}
	state := C.opus_decoder_create(48000, 1, nil)

	decoder.state = state
	return decoder
}

func (d Opus11Decoder) Decode(compressed []byte) ([]byte, int) {
	chardata := make([]C.uchar, len(compressed))

	for index, item := range compressed {
		chardata[index] = C.uchar(item)
	}

	iSampleRate := 48000
	iFrameSize := iSampleRate / 100
	iAudioBufferSize := iFrameSize * 12

	frame_size := 48000 * 120 / 1000

	out_size := frame_size * 1 * (16 / 8)

	out := make([]C.opus_int16, out_size)

	result := make([]byte, out_size*2)

	samples_or_error := C.opus_decode(d.state, &(chardata[0]), C.opus_int32(len(chardata)), &(out[0]), C.int(iAudioBufferSize), 0)

	for i := 0; i < out_size; i++ {
		s := out[i]
		result[2*i] = byte(s & 0xFF)
		result[2*i+1] = byte((s >> 8) & 0xFF)
	}

	/*
		for index, item := range out {

			result[index] = float32(item)
		}*/

	// fmt.Println(samples_or_error)

	return result, int(samples_or_error) * 2
}

func (d Opus11Decoder) Finalize() {
	C.opus_decoder_destroy(d.state)
}

func Main() {
	//160
	data := []byte{120, 0, 10, 96, 147, 81, 212, 187, 255, 195, 89, 23, 79, 47, 36, 78, 225, 92, 20, 150, 226, 67, 102, 25, 2, 202, 158, 77, 244, 215, 130, 118, 55, 181, 7, 19, 219, 60, 253, 223, 176, 167, 60, 87, 133, 228, 51, 69, 177, 93, 33, 122, 151, 186, 69, 173, 42, 77, 36, 171, 105, 225, 226, 6, 211, 62, 205, 233, 146, 249, 79, 247, 110, 239, 128, 139, 72, 1, 99, 6, 65, 55, 97, 6, 204, 227, 132, 215, 81, 25, 156, 182, 56, 63, 129, 234, 158, 245, 5, 221}

	// data = data[:100]

	chardata := make([]C.uchar, len(data))

	for index, item := range data {
		chardata[index] = C.uchar(item)
	}

	error := C.int(0)
	decoder := C.opus_decoder_create(48000, 2, &error)

	// fmt.Println(decoder, data)

	out := make([]C.float, 48000*2)

	length := C.opus_int32(len(chardata))

	fmt.Println("Length:", length)

	result := C.opus_decode_float(decoder, &(chardata[0]), length, &(out[0]), 48000, 0)

	fmt.Println("Decoded samples", result)

	// fmt.Println(out)
	// fmt.Println(error, C.OPUS_OK)
}

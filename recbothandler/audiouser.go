package recbothandler

type AudioUser struct {
	sound   *SoundQueue
	session int64
}

func NewAudioUser(session int64) *AudioUser {
	return &AudioUser{
		session: session,
		sound:   NewSoundQueue(),
	}
}

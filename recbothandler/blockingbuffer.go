package recbothandler

import "sync"

const SHOUT_BUFFER_SIZE = 8192

type BlockingBuffer struct {
	lock   sync.Locker
	size   int
	buffer []byte
	cond   *sync.Cond
}

func NewBlockingBuffer() BlockingBuffer {
	return BlockingBuffer{
		lock:   &sync.Mutex{},
		cond:   &sync.Cond{L: &sync.Mutex{}},
		size:   4800,
		buffer: make([]byte, 0),
	}
}

func (p *BlockingBuffer) add(buffer []byte) {
	p.lock.Lock()
	p.buffer = append(p.buffer, buffer...)
	if len(p.buffer) >= SHOUT_BUFFER_SIZE {
		p.cond.Broadcast()
	}
	p.lock.Unlock()
}

func (p *BlockingBuffer) get() (result []byte) {
	p.cond.L.Lock()
	if len(p.buffer) < SHOUT_BUFFER_SIZE {
		p.cond.Wait()
	}
	p.cond.L.Unlock()

	p.lock.Lock()
	result = p.buffer[:SHOUT_BUFFER_SIZE]
	p.buffer = p.buffer[SHOUT_BUFFER_SIZE:]
	p.lock.Unlock()
	return
}

func (p *BlockingBuffer) stop() {
	p.buffer = append(p.buffer, make([]byte, SHOUT_BUFFER_SIZE+1)...)
	//p.cond.L.Unlock()
	p.cond.Broadcast()
}

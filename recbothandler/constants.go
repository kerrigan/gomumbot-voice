package recbothandler

const (
	BUFFER                   = 0.1
	MUMBLE_SEQUENCE_DURATION = 10. / 1000
	RESOLUTION               = 10 // in ms
	FLOAT_RESOLUTION         = float64(RESOLUTION) / 1000
	BITRATE                  = 48000
	MONO_CHUNK_SIZE          = BITRATE * 2 * RESOLUTION / 1000
)

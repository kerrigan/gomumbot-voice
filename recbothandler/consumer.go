package recbothandler

import (
	"log"
	"os"
)

func (p *RecBotHandler) consumer() {
	file, err := os.Create("test.raw")

	defer file.Close()
	if err != nil {
		log.Fatal(err)
	}

	for p.running {
		select {
		case data := <-p.mixedQueue:
			converted := make([]byte, len(data)*2)
			for i := 0; i < len(data); i++ {
				converted[2*i] = byte(data[i] & 0xff)
				converted[2*i+1] = byte(data[i] >> 8)
			}
			file.Write(converted)
		case <-p.consumerCtrl:
			return
		}
	}
}

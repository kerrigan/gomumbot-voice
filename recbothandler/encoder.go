package recbothandler

import (
	"fmt"
	"log"
	"runtime"
)

//Encoding and streaming

func (p *RecBotHandler) Encoder() {
	runtime.LockOSThread()
	encoder, header, err := NewOggEncoder()
	if err != nil {
		log.Println("Can't init ogg encoder")
		p.Stop()
		return
	}

	log.Println("Write ogg header")

	p.oggBuffer.add(header)

	log.Println("Enc thread started")
	var data []byte
	var samples []int16

main:
	for {
		select {
		case samples = <-p.mixedQueue:

		case <-p.encoderCtrl:
			log.Println("Got encoderCtrl")
			break main
		}

		//encode in ogg vorbis
		data = make([]byte, len(samples)*2)
		for i := 0; i < len(samples); i++ {
			data[i*2+1] = byte(samples[i] >> 8)
			data[i*2] = byte(samples[i])

		}

		encoded := encoder.Encode(data)

		if len(encoded) == 0 {
			continue
		}
		p.oggBuffer.add(encoded)
	}

	encoded := encoder.Encode([]byte{})

	p.oggBuffer.add(encoded)

	encoder.Finalize()
	fmt.Println("Encoder stopped")
}

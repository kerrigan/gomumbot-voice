package recbothandler

import (
	"log"
	"os"
	"os/exec"
	"strconv"
	"strings"
	"syscall"
)

//FFMpeg runs ffmpeg worker
func (p *RecBotHandler) FFMpeg() {
	icecastString := "icecast://" + p.user + ":" + p.password + "@" + p.host + ":" + strconv.Itoa(p.port) + "/" + strings.TrimLeft(p.mount, "/")

	parts := strings.Fields("ffmpeg -f s16le -ar 48000 -ac 1 -i - -c:a libvorbis -ab 128k -ac 2 -ar 44100 -content_type application/ogg -f ogg " + icecastString)
	cmd := exec.Command(parts[0], parts[1:len(parts)]...)

	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	pipe, err := cmd.StdinPipe()

	if err != nil {
		log.Fatalln(err)
	}

	err = cmd.Start()

	if err != nil {
		log.Fatalln(err)
	}

	for p.running {
		select {
		case data := <-p.mixedQueue:
			converted := make([]byte, len(data)*2)
			for i := 0; i < len(data); i++ {
				converted[2*i] = byte(data[i] & 0xff)
				converted[2*i+1] = byte(data[i] >> 8)
			}
			pipe.Write(converted)
		case <-p.consumerCtrl:
			cmd.Process.Signal(syscall.SIGINT)
			return
		}
	}
}

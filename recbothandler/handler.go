package recbothandler

import (
	"fmt"
	"log"
	"runtime"
	"time"
)

type RecBotHandler struct {
	cursorTime float64
	running    bool
	users      map[int64]*AudioUser
	mixedQueue chan []int16
	nowTalking []int64

	consumerCtrl chan int
	pollerCtrl   chan int
	encoderCtrl  chan int
	streamerCtrl chan int

	oggBuffer BlockingBuffer

	host     string
	port     int
	user     string
	password string
	mount    string
}

func NewRecBotHandler(host, user, password, mount string, port int) *RecBotHandler {
	return &RecBotHandler{
		cursorTime: -1,
		running:    false,
		users:      make(map[int64]*AudioUser),
		mixedQueue: make(chan []int16, 4096*2),
		nowTalking: make([]int64, 0),

		consumerCtrl: make(chan int),

		pollerCtrl:   make(chan int),
		encoderCtrl:  make(chan int),
		streamerCtrl: make(chan int),

		oggBuffer: NewBlockingBuffer(),

		//	oggBuffer:     make([]byte, 0),
		//oggBufferLock: &sync.Mutex{},

		host:     host,
		user:     user,
		password: password,
		mount:    mount,
		port:     port,
	}
}

func (p *RecBotHandler) Run() {
	p.running = true
	//go p.consumer()
	go p.loop()
	//go p.Encoder()
	//go p.Streamer()
	go p.FFMpeg()
}

func (p *RecBotHandler) Stop() {
	p.running = false
	//p.oggBuffer.stop()
	p.consumerCtrl <- 1
	//p.encoderCtrl <- 1
	//p.streamerCtrl <- 1
	log.Println("RecBotHandler stopped")
}

func (p *RecBotHandler) IsRunning() bool {
	return p.running
}

func (p *RecBotHandler) ProcessVoice(session int64, sequence int64, last bool, data []byte) {
	if !p.running {
		return
	}

	newSize := len(data) / 2
	converted := make([]int16, newSize)
	for i := 0; i < newSize; i++ {
		converted[i] = int16(data[i*2]) | (int16(data[i*2+1]) << 8)
	}

	if _, ok := p.users[session]; !ok {
		p.users[session] = NewAudioUser(session)
	}

	p.users[session].sound.Add(converted, sequence)
	//fmt.Println("Session: ", session, "sequence: ", sequence, "samples: ", len(converted))

	//fmt.Println("Got samples ", len(converted))
}

func (p *RecBotHandler) loop() {
	runtime.LockOSThread()
	p.cursorTime = getTime() - BUFFER

	silent := make([]int16, 480)
	//silent := make([]int16, MONO_CHUNK_SIZE)

	currTime := time.Now()
	lastTime := currTime

	for p.running {

		//p.nowTalking = make([]int64, 0)
		//if p.cursorTime < getTime()-BUFFER {
		var baseSound []int16
		//session, user
		for _, user := range p.users {
			//Remove old samples
			for user.sound.IsSound() && user.sound.GetFirstSound().Time < p.cursorTime {
				fmt.Print("D")
				user.sound.GetSound(FLOAT_RESOLUTION)
			}

			if user.sound.IsSound() {
				if user.sound.GetFirstSound().Time >= p.cursorTime &&
					user.sound.GetFirstSound().Time < p.cursorTime+FLOAT_RESOLUTION {
					sound := user.sound.GetSound(FLOAT_RESOLUTION)

					//p.nowTalking = append(p.nowTalking, session)
					if len(baseSound) == 0 {
						baseSound = sound.Pcm
					} else {
						baseSound = soundAdd(baseSound, sound.Pcm)
					}
				}
			} else {
				//TODO: remove from captions
			}
		}

		if len(baseSound) > 0 {
			p.mixedQueue <- baseSound
		} else {
			//fmt.Print("S")
			p.mixedQueue <- silent
		}
		p.cursorTime += FLOAT_RESOLUTION

		lastTime = time.Now()

		currTime = lastTime
		if lastTime.Sub(currTime) < time.Millisecond*10 {
			time.Sleep(10 * time.Millisecond)
		}

		//fmt.Print("V")
		/*
			} else {
				//time.Sleep(FLOAT_RESOLUTION * time.Second)
				//fmt.Print("S")
				time.Sleep(10 * time.Millisecond)
			}
		*/

	}
}

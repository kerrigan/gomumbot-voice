package recbothandler

import "sync"

/*
type Queue struct {
	first *Element
	last  *Element
	size  int
	lock  sync.Locker
}

type Element struct {
	chunk *SoundChunk
	next  *Element
}

func NewQueue() *Queue {
	return &Queue{
		first: nil,
		last:  nil,
		size:  0,
		lock:  &sync.Mutex{},
	}
}

func (p *Queue) Size() int {
	return p.size
}

func (p *Queue) Append(chunk *SoundChunk) {
	p.lock.Lock()
	if p.size == 0 {
		p.first = &Element{chunk: chunk, next: nil}
		p.last = p.first
	} else {
		newLast := &Element{chunk: chunk, next: nil}
		p.last.next = newLast
		p.last = newLast
	}
	p.size++
	p.lock.Unlock()
}

func (p *Queue) Get(index int) (result *SoundChunk) {
	if index == 0 {
		result = p.first.chunk
	} else if index == p.size-1 {
		result = p.last.chunk
	} else {
		first := p.first
		for i := 1; i < index; i++ {
			first = first.next
		}
		result = first.chunk
	}
	return
}

func (p *Queue) Pop() *SoundChunk {
	p.lock.Lock()
	result := p.first.chunk
	p.first.next = p.first
	p.lock.Unlock()
	p.size--
	return result
}
*/

type Queue struct {
	queue []*SoundChunk
	lock  sync.Locker
}

func NewQueue() *Queue {
	return &Queue{
		queue: make([]*SoundChunk, 0),
		lock:  &sync.Mutex{},
	}
}

func (p *Queue) Get(index int) *SoundChunk {
	return p.queue[index]
}

func (p *Queue) Size() int {
	return len(p.queue)
}

func (p *Queue) Append(chunk *SoundChunk) {
	p.lock.Lock()
	p.queue = append(p.queue, chunk)
	p.lock.Unlock()
}

func (p *Queue) Put(index int, chunk *SoundChunk) {
	p.queue[index] = chunk
}

func (p *Queue) Pop() *SoundChunk {
	p.lock.Lock()
	result := p.queue[0]
	p.queue = p.queue[1:]
	//fmt.Println("Size:", len(p.queue))
	p.lock.Unlock()
	return result
}

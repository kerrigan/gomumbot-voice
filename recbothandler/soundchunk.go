package recbothandler

type SoundChunk struct {
	Timestamp float64
	Time      float64
	Pcm       []int16
	Duration  float64
	Sequence  int64
}

func NewSoundChunk(pcm []int16, sequence int64, time float64) *SoundChunk {
	return &SoundChunk{
		Pcm:       pcm,
		Time:      time,
		Sequence:  sequence,
		Duration:  float64(len(pcm)) / BITRATE,
		Timestamp: getTime(),
	}
}

func (p *SoundChunk) ExtractSound(duration float64) *SoundChunk {
	size := int(duration * BITRATE)

	result := &SoundChunk{
		Pcm:       p.Pcm[:size],
		Sequence:  p.Sequence,
		Time:      p.Time,
		Timestamp: p.Timestamp,
		Duration:  duration,
	}

	p.Pcm = p.Pcm[size:]
	p.Duration -= duration
	p.Time += duration

	return result
}

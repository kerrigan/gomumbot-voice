package recbothandler

import "sync"

type SoundQueue struct {
	queue         *Queue
	startSequence int64
	startTime     float64
	lock          sync.Locker
}

func NewSoundQueue() *SoundQueue {
	return &SoundQueue{
		queue:         NewQueue(),
		startSequence: -1,
		startTime:     -1,
		lock:          &sync.Mutex{},
	}
}

func (p *SoundQueue) Add(audio []int16, sequence int64) *SoundChunk {
	p.lock.Lock()

	var calculatedTime float64
	if p.startSequence == -1 || sequence <= p.startSequence {
		p.startTime = getTime()
		p.startSequence = sequence
		calculatedTime = p.startTime
	} else {
		calculatedTime = p.startTime + float64(sequence-p.startSequence)*MUMBLE_SEQUENCE_DURATION
	}

	newSound := NewSoundChunk(audio, sequence, calculatedTime)

	p.queue.Append(newSound)

	//fmt.Println("Queue:", *p.queue)

	//Sort chunks
	//Need only for udp packets, because tcp saves order
	//TODO: replace with fast implementation, now makes packet drops
	/*
		if p.queue.Size() > 1 && p.queue.Get(0).Time < p.queue.Get(1).Time {
			cpt := 0
			for cpt < p.queue.Size()-1 && p.queue.Get(cpt).Time < p.queue.Get(cpt+1).Time {
				tmp := p.queue.Get(cpt + 1)
				p.queue.Put(cpt+1, p.queue.Get(cpt))
				p.queue.Put(cpt, tmp)
			}
		}
	*/

	p.lock.Unlock()

	return newSound
}

func (p *SoundQueue) IsSound() bool {
	if p.queue.Size() > 0 {
		return true
	} else {
		return false
	}
}

func (p *SoundQueue) GetSound(duration float64) (result *SoundChunk) {
	p.lock.Lock()

	if p.queue.Size() > 0 {
		if duration < 0 || p.queue.Get(0).Duration <= duration {
			result = p.queue.Pop()
		} else {
			result = p.queue.Get(0).ExtractSound(duration)
		}
	} else {
		result = nil
	}
	p.lock.Unlock()

	return result
}

func (p *SoundQueue) GetFirstSound() *SoundChunk {
	return p.queue.Get(0)
}

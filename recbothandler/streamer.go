package recbothandler

import (
	"log"
	"runtime"

	"github.com/systemfreund/go-libshout"
)

//Streamer starts streaming thread which transfers data to icecast
func (p *RecBotHandler) Streamer() {
	runtime.LockOSThread()
	log.Println("Streamer started")

	s := shout.Shout{
		Host:     p.host,
		Port:     uint(p.port),
		User:     p.user,
		Password: p.password,
		Mount:    p.mount,
		Format:   shout.FORMAT_OGG,
		Protocol: shout.PROTOCOL_HTTP,
	}

	stream, err := s.Open()
	if err != nil {
		log.Println("Icecast connect failed, stopping handler:", err)
		p.Stop()
		return
	}
	log.Println("Streamer connected")

	defer s.Close()

outerloop:
	for {
		select {
		case <-p.streamerCtrl:
			log.Println("Got streamerCtrl")
			p.oggBuffer.stop()
			break outerloop
		default:
		}
		data := p.oggBuffer.get()
		stream <- data
	}

	log.Println("Streamer stopped")
}

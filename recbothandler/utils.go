package recbothandler

import "time"

/*
 */
//import C

func getTime() float64 {
	return float64(time.Now().UnixNano()/int64(time.Millisecond)/10) / 100.
}

func soundAdd(a1, a2 []int16) (result []int16) {
	cpt := 0
	size := len(a1)

	result = make([]int16, size)

	for cpt < size {
		if a1[cpt] > 0 && a2[cpt] > 0 {
			result[cpt] = (a1[cpt] + a2[cpt]) - ((a1[cpt] * a2[cpt]) / 32767)
		} else if a1[cpt] < 0 && a2[cpt] < 0 {
			result[cpt] = -((-a1[cpt] - a2[cpt]) - ((a1[cpt] * a2[cpt]) / 32767))
		} else {
			result[cpt] = a1[cpt] + a2[cpt]
		}

		cpt++
	}
	return result
}

package recbothandler

import (
	"errors"
	"math/rand"

	"github.com/grd/ogg"
	"github.com/grd/vorbis"
	"github.com/grd/vorbis/vorbisenc"
)

type OggEncoder struct {
	oss         ogg.StreamState // take physical pages, weld into a logical stream of packets
	og          ogg.Page        // one Ogg bitstream page. Vorbis packets are inside
	op          ogg.Packet      // one raw packet of data for decode
	vi          vorbis.Info     // struct that stores all the static vorbis bitstream settings
	vc          vorbis.Comment  // struct that stores all the user comments
	vd          vorbis.DspState // central working state for the packet PCM decoder
	vb          vorbis.Block    // local working space for packet PCM decode
	ret, i, val int
	eos         bool
}

func NewOggEncoder() (enc OggEncoder, headerData []byte, err error) {
	enc = OggEncoder{}

	enc.vi.Init()
	ret := vorbisenc.Init(&enc.vi, 2, 48000, 128000, 128000, 128000)

	if ret != 0 {
		err = errors.New("Can't init vorbis info")
		return
	}

	enc.vc.Init()
	enc.vc.AddTag("ENCODER", "encoder_example.go")
	enc.vc.AddTag("title", "golang mumble bot")
	// set up the analysis state and auxiliary encoding storage
	vorbis.AnalysisInit(&enc.vd, &enc.vi)
	enc.vb.Init(&enc.vd)
	// set up our packet stream encoder (oss)
	// pick a random serial number; that way we can more likely build
	// chained streams just by concatenation
	enc.oss.Init(rand.Int31())

	var (
		header     ogg.Packet
		headerComm ogg.Packet
		headerCode ogg.Packet
	)
	vorbis.AnalysisHeaderOut(&enc.vd, &enc.vc, &header, &headerComm, &headerCode)
	enc.oss.PacketIn(&header)
	enc.oss.PacketIn(&headerComm)
	enc.oss.PacketIn(&headerCode)

	for {
		result := enc.oss.Flush(&enc.og)
		if result == false {
			break
		}
		headerData = append(headerData, enc.og.Header...)
		headerData = append(headerData, enc.og.Body...)
	}

	return
}

func (p *OggEncoder) Encode(data []byte) (result []byte) {
	result = []byte{}
	//for !eos {

	SIZE := len(data)
	/*
		if SIZE != 1024 {
			log.Fatal("Wrong ogg vorbis chunk size")
		}
	*/
	var i int
	//channels := 2
	//ret, _ := os.Stdin.Read(readbuffer[0 : READ*4]) // stereo hardwired here

	ret := SIZE == 0
	if ret {
		// end of file. this can be done implicitly in the mainline,
		// but it's easier to see here in non-clever fashion.
		// Tell the library we're at end of stream so that it can handle
		// the last frame and mark end of stream in the output properly
		vorbis.AnalysisWrote(&p.vd, 0)
	} else {
		// data to encode
		// expose the buffer to submit data
		buffer := vorbis.AnalysisBuffer(&p.vd, SIZE)
		// uninterleave samples
		for i = 0; i < SIZE/2; i++ {
			buffer[0][i] = float32((int16(data[i*2+1])<<8)|(0x00ff&int16(data[i*2]))) / 32768.
			buffer[1][i] = buffer[0][i]
		}
		// tell the library how much we actually submitted
		vorbis.AnalysisWrote(&p.vd, SIZE/2)
	}
	// Vorbis does some data preanalysis, then divvies up blocks for
	// more involved (potentially parallel) processing. Get a single
	// block for encoding now
	for vorbis.AnalysisBlockOut(&p.vd, &p.vb) == 1 {
		// analysis, assume we want to use bitrate management
		vorbis.Analysis(&p.vb, nil)
		vorbis.BitrateAddBlock(&p.vb)
		for vorbis.BitrateFlushPacket(&p.vd, &p.op) != 0 {
			// weld the packet into the bitstream
			p.oss.PacketIn(&p.op)
			// write out pages (if any)
			for !p.eos {
				if p.oss.PageOut(&p.og) == false {
					break
				}

				result = append(result, p.og.Header...)
				result = append(result, p.og.Body...)
				// this could be set above, but for illustrative purposes, I do
				// it here (to show that vorbis does know where the stream ends)
				p.eos = p.og.Eos()
			}
		}
	}

	//}
	return
}

func (p *OggEncoder) Finalize() {
	p.vb.Clear()
	p.vd.Clear()
	p.vc.Clear()
	p.vi.Clear()
}

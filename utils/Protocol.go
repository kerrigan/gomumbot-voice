package utils

import (
	"log"

	opus "bitbucket.org/kerrigan/gomumbot/goopus_1_1"
	//"bytes"
	"fmt"
)

//var opusDecoder = opus.NewOpus11Decoder()

//var pcm =

type VoiceProcessor struct {
	opusDecoder *opus.Opus11Decoder
	outBuffer   []int16
	byteBuffer  []byte
}

func NewVoiceProcessor() *VoiceProcessor {
	decoder, err := opus.NewOpus11Decoder()

	if err != nil {
		log.Fatal("Failed to init opus:", err)
	}

	frame_size := 48000 * 120 / 1000

	out_size := frame_size * 1 * (16 / 8)

	p := &VoiceProcessor{
		opusDecoder: decoder,
		outBuffer:   make([]int16, out_size),
		byteBuffer:  make([]byte, out_size*2),
	}
	return p
}

// ProcessVoice parse mumble voice package, now only OPUS codec
func (p *VoiceProcessor) ProcessVoice(data []byte) (audio []byte, sequence int64, session int64, last bool) {
	defer func() {
		if r := recover(); r != nil {
			fmt.Println("Recovered in f", r)
		}
	}()

	shift := uint64(0)
	flags := data[shift]

	voiceType := (flags & 0xe0) >> 5

	if voiceType == 1 {
		fmt.Println("Ping found")
		return
	}

	shift++

	//session
	//session, sessionShift, err := DecodeVarint(data[shift : shift+10])
	session, sessionShift, err := DecodeVarint(data[shift:])
	if err != nil {
		panic(err)
	}

	shift += uint64(sessionShift)

	//sequence, sequenceShift, err := DecodeVarint(data[shift : shift+10])
	sequence, sequenceShift, err := DecodeVarint(data[shift:])
	if err != nil {
		panic(err)
	}
	shift += uint64(sequenceShift)

	finish := false

	audio = []byte{}
	//fmt.Println(">>>>>> Audio packet", len(data), ", session:", session, "sequence:", sequence)

	//
	//fmt.Println("Shift before voice", shift)
	for finish == false && shift < uint64(len(data)) {
		if voiceType != 4 {
			fmt.Println("TOO OLD")
			return
		}

		audioLength, audioLengthShift, err := DecodeVarint(data[shift:])
		if err != nil {
			fmt.Println("panic")
			panic(err)
		}

		shift += uint64(audioLengthShift)

		isLast := audioLength & 0x2000
		audioLength = audioLength & 0x1fff
		finish = isLast > 0

		chunk := data[shift : uint64(shift)+uint64(audioLength)]

		decodedSamples, err := p.opusDecoder.Decode(chunk, p.outBuffer)
		if err != nil {
			log.Fatal("Decoding error")
		}

		//decoded = p.outBuffer[:decodedBytes]
		//decoded = bytes.TrimRight(decoded, "\x00")
		decoded := p.outBuffer[:decodedSamples]

		for i := 0; i < len(decoded); i++ {
			s := decoded[i]
			p.byteBuffer[2*i] = byte(s & 0xFF)
			p.byteBuffer[2*i+1] = byte((s >> 8) & 0xFF)
		}

		audio = p.byteBuffer[:decodedSamples*2]
		shift += uint64(audioLength)
		//fmt.Println("Chunk found", audioLength)
		//fmt.Println("Shift after chunk", shift)
		last = finish
	}
	return
}

/*
func ProcessVoice(buffer []byte) {
	fmt.Println("Raw Voice buffer", buffer, len(buffer))

	fmt.Println("")

	//UDP Type/Target
	byteShift := 0
	udpType := buffer[byteShift]

	codecCode := (udpType & 0xe0) >> 5

	//   0 CELT Alpha encoded voice data
	//   1 Ping packet
	//   2 Speex encoded voice data
	//   3 CELT Beta encoded voice data
	//   4 OPUS encoded voice data
	//   5-7	Unused

	audioTarget := (udpType & 0x1f)

	//   0	Normal talking
	//   1	Whisper to channel
	//   2-30	Direct whisper (always 2 for incoming whisper)
	//   31	Server loopback

	if audioTarget != 0 {
		return
	}

	fmt.Println("Codec:", codecCode, "Target:", audioTarget)

	byteShift += 1

	udpSession, shift := DecodeVarint(buffer[byteShift:])
	byteShift += shift
	fmt.Println("UDP Type", udpType)

	if codecCode == 1 { //PING
		return
	}

	fmt.Println("UDP session", udpSession)

	udpSequence, shift := DecodeVarint(buffer[byteShift:])
	fmt.Println("UDP Sequence", udpSequence)
	byteShift += shift

	if codecCode != 4 { //Not OPUS, drop packet
		return
	}

	//Begin parse audio data

	//for { //TODO: выяснить про фреймы со звуком
	// fmt.Println("Buffer", byteShift, len(buffer))
	// fmt.Println(buffer)
	audioLength := int(buffer[byteShift] & 0x7f)

	lastFrame := ((buffer[byteShift] & 0x80) >> 7) == 0

	fmt.Println("Audio prefix", buffer[byteShift])

	if !lastFrame {
		fmt.Println(" not last frame")
	} else {
		fmt.Println(" last frame")
	}

	fmt.Println("Audio data length", audioLength)

	byteShift += 1

	// fmt.Println("Tail length", len(buffer[byteShift:byteShift+audioLength]))

	//byteShift+int(audioLength)
	audioData := buffer[byteShift : byteShift+audioLength] //I dunno, but looks like

	byteShift += audioLength
	//TODO: пакетов может быть больше одного, поэтому номера последовательности пропускаются
	//fmt.Println("Voice data:", audioData)

	//var decoded []C.uchar
	//var length_or_error int

	decoded, length_or_error := opusDecoder.Decode(audioData)

	if length_or_error > 0 {
		decoded = decoded[:length_or_error]
		encoderInput <- decoded
		// fmt.Println(oggencoder.Encode(decoded))
		// fmt.Println("Decoded length:", length_or_error, len(decoded))
	} else {
		fmt.Println("Decoded nothing, code:", length_or_error)
	}

	if !lastFrame {
		return
	}
	//} //TODO: выяснить про фреймы со звуком
	//end parse audio data

}
*/
